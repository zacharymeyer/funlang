package object

type Null struct{}

const NULL_OBJ = "NULL"

func (n *Null) Inspect() string {
	return "null"
}

func (n *Null) Type() ObjectType {
	return NULL_OBJ
}
