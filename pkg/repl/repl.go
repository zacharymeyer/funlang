package repl

import (
	"bufio"
	"fmt"
	"io"

	"gitlab.com/zacharymeyer/funlang/pkg/evaluator"
	"gitlab.com/zacharymeyer/funlang/pkg/lexer"
	"gitlab.com/zacharymeyer/funlang/pkg/object"
	"gitlab.com/zacharymeyer/funlang/pkg/parser"
)

const PROMPT = ">> "

func Start(in io.Reader, out io.Writer) {
	scanner := bufio.NewScanner(in)
	env := object.NewEnvironment()
	// Keep prompting the user for input until they enter 'CTRL + C' to exit
	for {
		fmt.Print(PROMPT)
		scanned := scanner.Scan()
		if !scanned {
			return
		}

		line := scanner.Text()
		l := lexer.New(line)
		p := parser.New(l)

		program := p.ParseProgram()
		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			continue
		}

		evaluated := evaluator.Eval(program, env)
		if evaluated != nil {
			io.WriteString(out, evaluated.Inspect())
			io.WriteString(out, "\n")
		}
	}
}

func printParserErrors(out io.Writer, errors []string) {
	for _, msg := range errors {
		io.WriteString(out, "\t"+msg+"\n")
	}
}
