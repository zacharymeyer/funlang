package main

import (
	"fmt"
	"os"

	"gitlab.com/zacharymeyer/funlang/pkg/repl"
)

func main() {
	fmt.Println("fun, a simple general purpose programming language.")
	repl.Start(os.Stdin, os.Stdout)
}
